import React from "react";
import { NavLink } from "react-router-dom";


const Navbar = () => {
    return(
        <ul>
            <li>
                <NavLink to='Home'>Home</NavLink>
            </li>
            <li>
                <NavLink to='Pokemon'>Pokemon</NavLink>              
            </li>
            <li>
                <NavLink to='Rick'>Rick</NavLink>
            </li>
        </ul>
        // <div style={{display: 'flex'}}>
        //     <a href="/Home">Home</a>;
        //     <a href="/Pokemon">Pokemon</a>;
        //     <a href="/Rick">Rick</a>;
        // </div>
    )
}

export default Navbar;