import Home from "./Home/Home";
import Pokemon from "./Pokemon/Pokemon";
import Rick from "./Rick/Rick";
import Navbar from "./Navbar/Navbar"
import NotFound from "./NotFound/NotFound";

export {
    Home,
    Pokemon,
    Rick,
    Navbar,
    NotFound,
}