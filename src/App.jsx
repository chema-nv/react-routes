import { BrowserRouter as Router, Route, Routes} from 'react-router-dom';
//importamos BrowserRouter como Router para usar Router en  App()
import { Home, Navbar, Pokemon, Rick,NotFound } from './componests';
import './App.scss';

function App() {
  return (
    <Router>
    <div className="app">
      <Navbar/>
      <Routes>
        <Route path='/Home' element={<Home/>}/>;
        <Route path='/Pokemon' element={<Pokemon/>}/>;
        <Route path='/rick' element={<Rick/>}/>;
        <Route path='*' element={<NotFound/>}/>
      </Routes>
      
    </div>
    </Router>
  );
}

export default App;
